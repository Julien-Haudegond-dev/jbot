# Working with `Rez`, THE Package Manager

## What is `Rez`?

`Rez` is a powerful package manager, initially developed for the VFX/Animation industry but that can be used for any kind of project.
A simple and short introduction of what is `Rez` can be found [here](https://rez.readthedocs.io/en/stable/), in its documentation.

## Why using `Rez`?

It may or it may not fit to your needs, but after giving `Rez` a try, it is difficult to come back to "traditional" package managers. There are many ways to work with `Rez`: from simple to very complex ones, there is something for everyone.

It can be used at its basic level, to resolve an environment matching the requirements of an application. Or it can used with complex configurations, dealing with version compatibilities and requiring other configurations... This is the **inception of environments**.

My personal uses are quite simple, nothing too fancy.

## Platforms

`Rez` is a cross-platform package manager. However, since the VFX industry is widely based upon Linux systems (see [VFX Platform](https://vfxplatform.com/)), its integration with Linux is really good. For Windows, it mainly works but not all the functionalities are available (default auto-completion, symlinks...).

## Installation and setup

### Installation

On Linux, it is pretty straight-forward. On Windows, it needs some additional steps to work just as well (and some of them are a bit tricky).

I am using Windows and that is why I write this document: to help people who can be in trouble when trying to install `Rez` on a Windows system. 

:pencil: I also work a lot on Linux systems and I really love `bash`. That is why I was accustomed to use `Git-Bash` on Windows. `Rez` is a package manager relying **a lot** on the shell you are using. And currently, based on my experience and issues on the GitHub, `Git-Bash` integration is not very reliable. That is the reason why I decided to switch from `Git-Bash` to `PowerShell Core`. At first, I was sad and frustrated for having lost `bash` syntax and some of its functionalities (quite restricted on Windows but still). And finally, it is quite fun to discover this modern `shell`. Moreover, since developed by Microsoft, its interactions with Windows and applications are really smooth. While using `Git-Bash`, you must ensure some paths are Windows-like when others are Unix-like, depending on their usages since "platform path syntax" and "shell path syntax" are different: it leads to some unnecessary headaches. With `PowerShell`, "platform path syntax" and "shell path syntax" are the same so it saves so much energy.
In a nutshell (without any pun :stuck_out_tongue_winking_eye: ), I am using `PowerShell Core` as my main shell on Windows and `Rez` is configured to work with it, and not `Git-Bash` or other old `cmd`.

Basic installation guide can be found [here](https://rez.readthedocs.io/en/stable/installation.html).

For my personal use, I defined the following structure for my `Rez` workflow:

- `D:\rez` (where things related to `Rez` should be)
  - `pkgs` (where the packages will be released)
    - `int` (internal packages - the ones I develop myself)
    - `ext` (external packages - the ones which come from outside: libs, applications, etc)
  - `soft` (where `Rez` software things should be)
    - `{version}` (directory of the installed version of `Rez`)
    - `rezconfig_overrides.py` (overrides for the default `Rez` configuration)

So, when I installed `Rez` thanks to the command `python ./install.py`, I used the path `D:\rez\soft\{version}` as the directory of installation: `python .\install.py D:\rez\soft\{version}`

Now, `Rez` is installed. But we cannot use it yet, since as mentioned by the installer in the shell, we must add it to `PATH`.

Two possibilities here: either by using "Windows System/User Environment Variables GUI" or dynamically for each new shell. I prefer the second one since it is all about personal configuration files: it is too easy to forget what was declared via the Windows Settings and things can then be messed up.

Using PowerShell, we can set a startup script which is executed every time a new shell is opened (on Linux, it is the equivalent of `bashrc`/`bash_profile`): see this [documentation](https://learn.microsoft.com/fr-fr/powershell/module/microsoft.powershell.core/about/about_profiles?view=powershell-7.4).

It gives me something like this:

```
$env:PATH += ";D:\rez\soft\3.0.0\Scripts\rez"
$env:REZ_CONFIG_FILE = "D:\rez\soft\rezconfig_overrides.py"
```

As we can see, there is also the variable `REZ_CONFIG_FILE`. We will come back to it later.

Now, if we open a new shell, we can execute the command `rez`! :tada:

### Setup

For `Rez` to work as expected, we must tell him where to deploy and find packages. For that, we can override the settings provided by the configuration. We must not write directly inside the installed `rezconfig.py`. The best thing to do is to create a Python file for storing those overrides, and only those ones. This is my file: `D:\rez\soft\rezconfig_overrides.py`. By setting the `REZ_CONFIG_FILE` to it, we tell `Rez` to use those settings instead of the ones declared in its root configuration.

My config file looks like this:

```python
import os


packages_path = [
    f"{os.environ['USERPROFILE']}\\rez_pkgs",
    "D:\\rez\\pkgs\\int",
    "D:\\rez\\pkgs\\ext",
]

local_packages_path = f"{os.environ['USERPROFILE']}\\rez_pkgs"

release_packages_path = "D:\\rez\\pkgs\\int"

allow_unversioned_packages = False

default_shell = "pwsh"  # To load PowerShell Core and not PowerShell Desktop.

plugins = {
    "shell": {
        "pwsh": {
            "execution_policy": "RemoteSigned",
        },
    },
}

pathed_env_vars_exceptions = [
    "CMAKE_MODULE_PATH",
]
```

For more details on those settings, it is [there](https://rez.readthedocs.io/en/stable/configuring_rez.html).

Briefly:
- `packages_path` defines the directories where `Rez` can look for a package.
- `local_packages_path` defines the directory where `Rez` can build/search **local** packages (during development).
- `release_packages_path` defines the default directory where `Rez` can release packages.
- `allow_unversioned_packages` forces packages to have a version (or not).
- `default_shell` defines the shell `Rez` should use.
- `plugins` allows to set particular settings to the plugins used by `Rez`, such as the shell. In this case, overriding `execution_policy` allows to execute `Rez` scripts.
- `pathed_env_vars_exceptions` is not referenced by the official documentation, we will see later on why.

### Installation of default packages

We can install default packages with the `rez bind` command.

For me:
- `rez bind os -i "D:\rez\pkgs\ext"`: installation of `os`, `platform` and `arch` packages.

With the same script we used to install `Rez`, we can install `Rez` as a `Rez` package! Like so, its Python API can be used in scripts:
`python .\install.py --as-rez-package "D:\rez\pkgs\ext"`

We can use `rez pip` to install any imaginable Python package: 
- `rez pip --python-version 3.10 -i -r -p "D:\rez\pkgs\ext" numpy`: installation of `numpy` and its dependencies for `Python 3.10`.

:warning: For `rez pip` to work as expected with the `--python-version` flag, you must have a `Rez` package of `Python` (with that exact version) (see below). If not, the Python version will be the one of the Python used to install `Rez`.

### Installation of useful packages

To me, there are three packages that are really useful to install as `Rez` packages. Actually, they are not "true" packages but more redirection tricks (we are on Windows, as you remember).

- `Python` is the first important one: here is [my package](https://gitlab.com/mc-risson-dev/rez/python).
- `CMake` is the second one, since we will use it a lot for package building processes: here is [my package](https://gitlab.com/mc-risson-dev/rez/cmake).
- `MSVC` is the compiler we use on Windows. It is quite annoying to use outside of dedicated Microsoft shells. However, they are some workarounds using provided batch scripts. [The package I made](https://gitlab.com/mc-risson-dev/rez/msvc) uses this workaround to make compiling executables available in our PowerShell sessions.

Great! Now, we are almost over.

Yep, we will use `CMake` and `MSVC` for the `Rez` building process. But at this point, it will not work. Because `CMake` had the super good idea to ask a mix of Windows and Unix paths when declaring the variable `CMAKE_MODULE_PATH`, see [here](https://cmake.org/cmake/help/latest/variable/CMAKE_MODULE_PATH.html). If we use the current version of `Rez` (`3.0.0` at the moment I am writing this) with its default config, it cannot work. The issue has already been raised several times, some Pull Requests were made to solve that but it takes a lot of time for the maintainers to ensure it is reliable. So, at this time, with the official release, we are blocked.

Personally, I did not want to wait more time for a usable version of `Rez` on my Windows system. I met that exact same problem one year ago. I know it is difficult to properly solve this problem: maintaining a cross-platform project dealing a lot with file paths is a nightmare. So I found a way to this issue [by myself](https://github.com/mc-risson/rez/pull/1), for my own usage. Since I do not care about other platforms or shells, using this workaround leaves me working with `Rez` and `CMake` seamlessly.

This is the explanation of the setting `pathed_env_vars_exceptions`. 

## Using `Rez`

Now, it is time to use `Rez`! And there is no better way to learn than trying and reading the [documentation](https://rez.readthedocs.io/en/stable/getting_started.html)! :wink:

## Additional info

- [Allow to configure execution policy for Powershell/pwsh plugins](https://github.com/AcademySoftwareFoundation/rez/issues/1504)
- [CMake breaks due to windows penchant for escape characters as path separators](https://github.com/AcademySoftwareFoundation/rez/issues/1321)
- [shebang and executable paths incorrect in gitbash](https://github.com/AcademySoftwareFoundation/rez/issues/1302)
- [Windows Gitbash Support Fixes #1302](https://github.com/AcademySoftwareFoundation/rez/pull/1475)
- [Windows PySide not working with Python-3.8+](https://github.com/AcademySoftwareFoundation/rez/issues/1221)